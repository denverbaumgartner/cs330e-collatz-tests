#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    # -- initial unit tests --

    def test_read_s1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    # -- corner cases --

    # full range of acceptable values
    def test_read_c1(self):
        s = "1 999999\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 999999)

    # same value for min/max low
    def test_read_c2(self):
        s = "1 1\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 1)

    # same value for min/max high length
    def test_read_c3(self):
        s = "999999 999999\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  999999)
        self.assertEqual(j, 999999)

    # ----
    # eval
    # ----

    # -- initial unit tests --

    def test_eval_s1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_s2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_s3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_s4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    # -- corner cases --

    # full range values
    def test_eval_c1(self):
        v = collatz_eval(1, 999999)
        self.assertEqual(v, 525)

    # same min/max values low
    def test_eval_c2(self):
        v = collatz_eval(1, 1)
        self.assertEqual(v, 1)

    # same min/max values high
    def test_eval_c3(self):
        v = collatz_eval(999999, 999999)
        self.assertEqual(v, 259)

    # reversed values
    def test_eval_f1(self):
        v = collatz_eval(10, 1)
        self.assertEqual(v, 20)

    # -----
    # print
    # -----

    # -- initial unit tests --

    # simple values
    def test_print_s1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    # -- corner cases --

    # full range values
    def test_print_c1(self):
        w = StringIO()
        collatz_print(w, 1, 999999, 525)
        self.assertEqual(w.getvalue(), "1 999999 525\n")

    # same low values entered
    def test_print_c2(self):
        w = StringIO()
        collatz_print(w, 1, 1, 1)
        self.assertEqual(w.getvalue(), "1 1 1\n")

    # same high values entered
    def test_print_c3(self):
        w = StringIO()
        collatz_print(w, 999999, 999999, 259)
        self.assertEqual(w.getvalue(), "999999 999999 259\n")


    # -----
    # solve
    # -----

    # -- initial unit tests --
    # Simple Happy Path
    def test_solve_s1(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    # -- corner cases --

    # repeated tests
    def test_solve_c1(self):
        r = StringIO("1 20\n1 20\n1 20\n1 20\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 20 21\n1 20 21\n1 20 21\n1 20 21\n")

    # large inputs
    def test_solve_c2(self):
        r = StringIO("999999 999999\n999888 999999\n888999 999999\n888888 999999\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "999999 999999 259\n999888 999999 259\n888999 999999 507\n888888 999999 507\n")


# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
